#!/usr/bin/python3

# Add randomness to the Magic 8 Ball
import random
import re

name = ''
question = str(input('What is your question?: '))

# Check if integer with regex
integer_format = re.compile(r'^\-?[1-9][0-9]*$')
is_integer = re.match(integer_format, question)


if question == '':
    exit('Could not determine your question')
elif is_integer:
    exit(question + ' is a number, please try again.')
else:
    if not name:
        print('\u0332'.join('Question: {}\n'.format(question)))
    else:
        print()
        print('\u0332'.join('{} asks: {}\n'.format(name, question)))

answer = ''
random_number = random.randint(1,10)

answer = {
    1 : 'Yes Definitely.',
    2 : 'It is decidedly so.',
    3 : 'Without a doubt.',
    4 : 'Reply hazy, try again.',
    5 : 'Ask again later.',
    6 : 'Better not tell you now.',
    7 : 'My sources say no.',
    8 : 'Outlook not so good.',
    9 : 'Very doubtful.',
    10: 'It\'s almost certain.'
}



print('Magic 8 Ball\'s answer: ' + answer[random_number])